﻿using SmallWorld.src.controllers;
using SmallWorld.src.interfaces;
using SmallWorld.src.model.animalsType;
using SmallWorld.src.model.dietType;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace SmallWorld.src.views
{
    public partial class CRUDView : Form
    {
        private AnimalController animalController = AnimalController.GetController();

        public CRUDView()
        {
            InitializeComponent();
            CbbDiet.Items.Add(new Carnivore());
            CbbDiet.Items.Add(new HervivorousDiet());
            CbbDiet.Items.Add(new Omnivorous());
            CbbDiet.SelectedIndex = 0;

        }

        private void BtnCreateAnimal_Click(object sender, EventArgs e)
        {
            animalController.AddAnimal
                (new Animal(new Terrestial(),
                new Carni()
                    TxtSpecie.Text, 
                    Convert.ToDouble(TxtWeight.Text), 
                    Convert.ToInt32(TxtAge.Text), 
                    (IDiet)CbbDiet.SelectedItem));
            
            DgvAnimals.DataSource = animalController.getAnimals();
        }
    }
}
