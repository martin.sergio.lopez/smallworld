﻿using SmallWorld.src.interfaces;
using SmallWorld.Src.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.src.model.dietType
{
    internal class HervivorousDiet : IDiet
    {
        public bool CanEat(IFood food)
        {
            return food is Vegetable;
        }
        public override string ToString()
        {
            return "Hervívoro";
        }
    }
}
