﻿using SmallWorld.src.interfaces;
using SmallWorld.Src.Model.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.Model.Diets
{
    internal class Carnivore : IDiet
    {
        public bool CanEat(IFood food)
        {
            return food is Animal;
        }

        public override string ToString()
        {
            return "Carnívoro";
        }
    }
}
