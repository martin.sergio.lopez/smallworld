﻿using SmallWorld.src.interfaces;
using SmallWorld.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.src.model.animalsType
{
    internal class Animal : IPositionable, IEntity, IAnimal, IFood
    {
        //ATRIBUTOS
        int PosX;
        int PosY;
        IEnvironment Environment;
        IDiet Diet;
        string Specie;
        float Weight;
        float Age;

        //Métodos
        public Animal(IEnvironment environment, IDiet diet, string specie, float weight, float age)
        {
            PositionX = 0;
            PositionY = 0;
            Environment = environment;
            Diet = diet;
            Specie = specie;
            Weight = weight;
            Age = age;
        }
        public Animal(int posX, int posY, IEnvironment environment, IDiet diet, string specie, float weight, float age)
        {
            PositionX = posX;
            PositionY = posY;
            Environment = environment;
            Diet = diet;
            Specie = specie;
            Weight = weight;
            Age = age;
        }

        //Propiedades
        public int PositionX
        {
            set { if (value > 0) PosX = value; }
            get { return PosX; }
        }
        public int PositionY
        {
            set { if (value > 0) PosY = value; }
            get { return PosY; }
        }

        //Implementación de IPositionable
        public int GetPosX()
        {
            throw new NotImplementedException();
        }
        public int GetPosY()
        {
            throw new NotImplementedException();
        }
        public bool Move(ITerrain terrain)
        {
            throw new NotImplementedException();
        }

        //Implementación de IEntity
        public IEnvironment GetEnvironment()
        {
            throw new NotImplementedException();
        }
        public IDiet GetDiet()
        {
            throw new NotImplementedException();
        }
        public bool Feed(IFood food)
        {
            throw new NotImplementedException();
        }
        public bool Sleep()
        {
            throw new NotImplementedException();
        }

        //Implementación de IAnimal
        public string GetSpecie()
        {
            throw new NotImplementedException();
        }
        public float GetAge()
        {
            throw new NotImplementedException();
        }        
        public float GetWeight()
        {
            throw new NotImplementedException();
        }
        //Implementación de IAnimal
        public string GetName()
        {
            throw new NotImplementedException();
        }

        public int GetCalories()
        {
            throw new NotImplementedException();
        }
    }
}
