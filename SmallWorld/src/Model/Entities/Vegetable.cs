﻿using SmallWorld.src.interfaces;
using SmallWorld.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.Src.Model.Entities
{
    internal class Vegetable : IPositionable, IEntity, IFood
    {
        private int Calories;
        private string Name;

        public Vegetable(int calories, string name)
        {
            Calories = calories;
            Name = name;
        }        

        //Implementación de IPosicionable
        public int GetPosX()
        {
            throw new NotImplementedException();
        }

        public int GetPosY()
        {
            throw new NotImplementedException();
        }

        public bool Move(ITerrain terrain)
        {
            throw new NotImplementedException();
        }

        public bool Sleep()
        {
            throw new NotImplementedException();
        }

        //Implementación de IEntity
        public bool Feed(IFood food)
        {
            throw new NotImplementedException();
        }

        public int GetCalories()
        {
            return Calories;
        }

        public IDiet GetDiet()
        {
            throw new NotImplementedException();
        }

        public IEnvironment GetEnvironment()
        {
            throw new NotImplementedException();
        }

        public string GetName()
        {
            return Name;
        }
    }
}
