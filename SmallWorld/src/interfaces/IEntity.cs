﻿using SmallWorld.src.interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.src.Interfaces
{
    internal interface IEntity
    {
        IEnvironment GetEnvironment();
        IDiet GetDiet();
        bool Feed(IFood food);
        bool Sleep();

    }
}
