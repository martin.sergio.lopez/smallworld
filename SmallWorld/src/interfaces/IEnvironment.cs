﻿using SmallWorld.src.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.src.interfaces
{
    internal interface IEnvironment
    {
        bool CanMoveThrough(ITerrain terrain);
    }
}
