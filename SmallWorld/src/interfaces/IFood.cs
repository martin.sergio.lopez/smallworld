﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmallWorld.src.interfaces
{
    internal interface IFood
    {
        string GetName();
        int GetCalories();
    }
}
