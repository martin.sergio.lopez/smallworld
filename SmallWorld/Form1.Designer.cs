﻿namespace SmallWorld
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            lblAnimalMessage = new Label();
            BtnAnimals = new Button();
            SuspendLayout();
            // 
            // lblAnimalMessage
            // 
            lblAnimalMessage.AutoSize = true;
            lblAnimalMessage.Font = new Font("Segoe UI", 29F, FontStyle.Regular, GraphicsUnit.Point);
            lblAnimalMessage.ForeColor = Color.FromArgb(224, 224, 224);
            lblAnimalMessage.Location = new Point(364, 255);
            lblAnimalMessage.Name = "lblAnimalMessage";
            lblAnimalMessage.Size = new Size(293, 52);
            lblAnimalMessage.TabIndex = 0;
            lblAnimalMessage.Text = "AnimalMessage";
            // 
            // BtnAnimals
            // 
            BtnAnimals.FlatStyle = FlatStyle.Flat;
            BtnAnimals.ForeColor = Color.FromArgb(224, 224, 224);
            BtnAnimals.Location = new Point(12, 616);
            BtnAnimals.Name = "BtnAnimals";
            BtnAnimals.Size = new Size(104, 26);
            BtnAnimals.TabIndex = 10;
            BtnAnimals.Text = "Animals";
            BtnAnimals.UseVisualStyleBackColor = true;
            BtnAnimals.Click += BtnAnimals_Click;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = Color.FromArgb(64, 64, 64);
            ClientSize = new Size(1067, 654);
            Controls.Add(BtnAnimals);
            Controls.Add(lblAnimalMessage);
            FormBorderStyle = FormBorderStyle.None;
            Name = "Form1";
            Text = "Form1";
            Load += Form1_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Label lblAnimalMessage;
        private Button BtnAnimals;
    }
}